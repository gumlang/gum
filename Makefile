export platform ?= auto
export config ?= debug
export cc ?= gcc
export ar ?= gcc-ar
export ld ?= gcc

default: setup
	tup $(TUP_FLAGS)
verbose:
	make default TUP_FLAGS=--verbose
setup:
	echo PLATFORM = $(platform) > config.tup
	echo CONFIG = $(config) >> config.tup
	echo CC = $(cc) >> config.tup
	echo AR = $(ar) >> config.tup
	echo LD = $(ld) >> config.tup
clean:
	git clean -fdX
.PHONY: default verbose setup clean